ghex (46.2-1) unstable; urgency=medium

  * New upstream release
    - Fix freeze with Copy Special (LP: #2067596)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 10 Feb 2025 08:14:43 -0500

ghex (46.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 15 Nov 2024 17:47:05 -0500

ghex (46.0-1) unstable; urgency=medium

  * New upstream release
  * Bump minimum libadwaita to 1.2
  * debian/libgtkhex-4-1.symbols: Add new symbols

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 27 Mar 2024 18:56:41 -0400

ghex (45.1-1) unstable; urgency=medium

  * New upstream release
  * Stop using debian/control.in and dh-sequence-gnome

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 27 Nov 2023 09:00:10 -0500

ghex (45.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 25 Oct 2023 12:59:09 -0400

ghex (45~beta-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 23 Aug 2023 10:12:28 -0400

ghex (44.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 18 Aug 2023 08:01:52 -0400

ghex (44.1-2) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 21 Jul 2023 12:19:13 -0400

ghex (44.1-1) experimental; urgency=medium

  * Team upload
  * New upstream release
    - hex-dialog: Prevent decimal display widgets from getting truncated
      (Jordan Christiansen)

 -- Amin Bandali <bandali@ubuntu.com>  Thu, 27 Apr 2023 14:41:03 -0400

ghex (44.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 27 Mar 2023 16:27:07 -0400

ghex (44~rc-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 10 Mar 2023 07:45:38 -0500

ghex (44~beta-1) experimental; urgency=medium

  * New upstream release
  * debian/libgtkhex-4-1.symbols: Add new symbol

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 14 Feb 2023 09:09:40 -0500

ghex (43.1-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.6.2

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 09 Feb 2023 12:13:10 -0500

ghex (43.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 06 Oct 2022 15:46:40 -0400

ghex (43~rc-1) unstable; urgency=medium

  * New upstream release
  * Drop all patches: no longer needed

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 21 Sep 2022 15:48:13 -0400

ghex (43~alpha-4) unstable; urgency=medium

  * Cherry-pick patch to fix file conflict with gnome-text-editor
    (Closes: #1018731)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 07 Sep 2022 17:38:43 -0400

ghex (43~alpha-3) unstable; urgency=medium

  * debian/control.in: Add Breaks/Replaces against libgtk-4-1 (Closes: #1018686)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 28 Aug 2022 22:21:53 -0400

ghex (43~alpha-2) unstable; urgency=medium

  * Rebuild after NEW acceptance

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 28 Aug 2022 16:27:51 -0400

ghex (43~alpha-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Rename library for soname bump
  * Update symbols file
  * Update install file
  * libadwaita-1-dev is build dependency

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Tue, 23 Aug 2022 18:49:47 -0300

ghex (42.3-1) unstable; urgency=medium

  * New upstream release

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Mon, 13 Jun 2022 21:27:10 +0200

ghex (42.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 26 Apr 2022 17:16:41 -0400

ghex (42.1-3) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 21 Apr 2022 12:06:13 -0400

ghex (42.1-2) experimental; urgency=medium

  * Release to unstable

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 21 Apr 2022 11:46:26 -0400

ghex (42.1-1) experimental; urgency=medium

  * New upstream release
  * Build with gtk4
  * Rename libgtkhex-3-0 & libgtkhex-3-dev to libgtkhex-4-0 & libgtkhex-4-dev
  * debian/libgtkhex-4-0.install: Install libhex-buffer-mmap.so
  * debian/control.in: Bump minimum meson & glib
  * debian/control.in: Build-Depend on appstream-util for build test
  * debian/copyright: Convert to 1.0 format
  * Bump debhelper-compat to 13
  * Add debian/upstream/metadata
  * Set Rules-Requires-Root: no
  * Add patch to work around meson bug 1008189

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 18 Apr 2022 15:19:27 -0400

ghex (3.41.1-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 04 Dec 2021 16:24:57 -0500

ghex (3.41.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum meson to 0.50.0
  * debian/ghex.docs: AUTHORS was removed & README is now README.md
  * debian/rules: Simplify a bit
  * Bump Standards-Version to 4.6.0

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 24 Sep 2021 18:02:04 -0400

ghex (3.18.4-1) unstable; urgency=medium

  * New upstream release
  * Build with meson (Closes: #829823)
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome instead of gnome-pkg-tools
  * Bump Standards-Version to 4.4.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 22 Sep 2019 12:57:17 -0400

ghex (3.18.3-5) unstable; urgency=medium

  * Add -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 24 Dec 2018 09:34:20 -0500

ghex (3.18.3-4) unstable; urgency=medium

  * Add X-Ubuntu-Use-Langpack to opt in to Ubuntu language pack handling
    (LP: #1779574)
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 04 Sep 2018 15:05:10 -0400

ghex (3.18.3-3) unstable; urgency=medium

  * Update Vcs fields for migration to https://salsa.debian.org/
  * Bump debhelper compat to 11

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 03 Feb 2018 21:48:54 -0500

ghex (3.18.3-2) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 16 Dec 2017 11:46:22 -0500

ghex (3.18.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper compat level to 10.
  * Add Build-Depends on gnome-common, required by autoreconf.

 -- Michael Biebl <biebl@debian.org>  Wed, 12 Oct 2016 15:50:03 +0200

ghex (3.18.2-2) unstable; urgency=medium

  * Add Build-Depends on desktop-file-utils for desktop-file-validate which
    is used during "make check".

 -- Michael Biebl <biebl@debian.org>  Tue, 21 Jun 2016 18:42:48 +0200

ghex (3.18.2-1) unstable; urgency=medium

  * New upstream release.
  * Convert from cdbs to dh.

 -- Michael Biebl <biebl@debian.org>  Sat, 18 Jun 2016 23:27:45 +0200

ghex (3.18.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Bump debhelper compatibility level to 9.
  * Convert to multiarch.

 -- Michael Biebl <biebl@debian.org>  Wed, 11 May 2016 15:31:27 +0200

ghex (3.18.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove Debian menu entry.
  * Bump Standards-Version to 3.9.6.

 -- Michael Biebl <biebl@debian.org>  Mon, 28 Sep 2015 15:26:25 +0200

ghex (3.10.1-1) unstable; urgency=low

  [ Jeremy Bicha ]
  * Don't build-depend on scrollkeeper

  [ Michael Biebl ]
  * New upstream release.
  * Bump Standards-Version to 3.9.5.
  * Update homepage URL.

 -- Michael Biebl <biebl@debian.org>  Tue, 02 Sep 2014 23:17:59 +0200

ghex (3.8.1-1) unstable; urgency=low

  * New upstream release.
  * Port to new documentation infrastructure. Use yelp-tools instead of
    gnome-doc-utils.
  * Bump Standards-Version to 3.9.4. No further changes.

 -- Michael Biebl <biebl@debian.org>  Fri, 14 Jun 2013 00:05:20 +0200

ghex (3.4.1-1) unstable; urgency=low

  * New upstream translation release.

 -- Michael Biebl <biebl@debian.org>  Tue, 15 May 2012 15:23:18 +0200

ghex (3.4.0-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Track .xz tarballs.
  * Bump Build-Depends on libglib2.0-dev to (>= 2.31.10).
  * Bump Build-Depends on libgtk-3-dev to (>= 3.3.8).
  * Bump Standards-Version to 3.9.3.
  * Add symbols file for libgtkhex-3-0.

 -- Michael Biebl <biebl@debian.org>  Thu, 19 Apr 2012 16:53:32 +0200

ghex (3.0.0-1) unstable; urgency=low

  * New upstream release.
    - Fixes format string error. (Closes: #643386)
    - Ported to GNOME 3 / GTK 3 removing dependencies on deprecated libraries
      like libgnomeprint. (Closes: #542554)
  * Update watch file.
  * Switch to dpkg source format 3.0 (quilt).
    - Add debian/source/format.
    - Remove simple-patchsys.mk include.
  * Bump debhelper compatibility level to 8.
    - Bump Build-Depends on debhelper.
    - Strip debian/tmp/ from .install files.
  * debian/control.in:
    - Drop Build-Depends on libgnomeui-dev, libgnomeprintui2.2-dev,
      libpopt-dev and gconf2.
    - Update Build-Depends on libgtk2.0-dev to libgtk-3-dev (>= 3.0.0).
    - Update Build-Depends on libgail-dev to libgail-3-dev.
    - Bump Build-Depends on intltool to (>= 0.41.1).
    - Bump Build-Depends on gnome-doc-utils to (>= 0.9.0).
    - Add Build-Depends on libglib2.0-dev (>= 2.26).
    - Bump Standards-Version to 3.9.2.
    - Add Vcs-* fields.
    - Add Homepage field.
    - Set pkg-gnome-maintainers@lists.alioth.debian.org as Maintainer.
  * Update for libgtkhex SONAME bump.
    - Rename libgtkhex0 → libgtkhex-3-0.
    - Rename libgtkhex0-dev → libgtkhex-3-dev.
    - Bump minimum shlibs version to 3.0.0.
  * The main binary was renamed from ghex2 → ghex.
    - Rename and update manpage.
    - Update menu file.
  * debian/rules:
    - Remove some cruft which is no longer necessary.

 -- Michael Biebl <biebl@debian.org>  Fri, 07 Oct 2011 03:04:20 +0200

ghex (2.24.0-1) unstable; urgency=low

  [ Loic Minier ]
  * Properly anchor package name regexp.
  * Fix section of man page.

  [ Sebastian Dröge ]
  * New upstream stable release.
  * debian/control.in:
    + Update Standards-Version to 3.8.0 (no additional changes needed).
    + Build depend on intltool.

 -- Sebastian Dröge <slomo@debian.org>  Thu, 19 Feb 2009 07:30:06 +0100

ghex (2.22.0-1) unstable; urgency=low

  * New upstream stable release.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 11 Apr 2008 14:47:24 +0200

ghex (2.21.92-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Sebastian Dröge <slomo@debian.org>  Wed, 27 Feb 2008 08:49:08 +0100

ghex (2.21.90-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Sebastian Dröge <slomo@debian.org>  Thu, 31 Jan 2008 10:02:01 +0100

ghex (2.21.4-1) unstable; urgency=low

  * New upstream development release; API additions; bug fixes; translation
    update.
    - Targetted at unstable since the changes seem stable.
    - Bump up shlibs to >= 2.21.4.
    - Fixes incorrect version in the .pc file; thanks Luca Bruno.
    - Update watch file to track all versions.
  * Don't hardcode the lib package name.
  * Cleanup rules.
  * Wrap build-deps and deps.

 -- Loic Minier <lool@dooz.org>  Fri, 18 Jan 2008 15:34:01 +0100

ghex (2.20.1-1) unstable; urgency=low

  [ Loic Minier ]
  * Stop shipping *.la files in libgtkhex0-dev.
  * Add a get-orig-source target to retrieve the upstream tarball.

  [ Kilian Krause ]
  * Use binary:version and source:Version for binnNMU-safe uploads as
    added in dpkg-dev 1.13.19. Add to Build-Depends accordingly

  [ Sebastian Dröge ]
  * New upstream release:
    + Fixes crash while searching an ASCII string (Closes: #416480).
    + Fixes freeze when search reaches the end of a file (Closes: #373712).
    + debian/control.in:
      + Update build dependencies.
      + Let the -dev package depend on libgail-dev, libgtk2.0-dev and
        libatk1.0-dev.
  * debian/control.in,
    debian/compat:
    + Update Standards-Version to 3.7.3 (no additional changes needed).
    + Update to debhelper compat level 5.
  * debian/watch:
    + Updated for the new version.
  * debian/ghex.menu:
    + Fix menu file section.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 10 Dec 2007 08:14:05 +0100

ghex (2.8.2-3) unstable; urgency=low

  * Revert changes of 2.8.2-2 as release team explained that a simple
    re-upload is enough for non-bin-NMU-able packages, and that bin-NMU-able
    packages should be taken care of by them.
  * Move the ghex package to the end of control to ensure it's built after
    libghex. (Closes: #364862)
    [debian/control, debian/control.in]

 -- Loic Minier <lool@dooz.org>  Wed, 26 Apr 2006 11:49:52 +0200

ghex (2.8.2-2) unstable; urgency=low

  * Add empty patches dir.
    [debian/patches]
  * Bump up build-deps to libgnomeui-dev >= 2.14.1-1, libgnomeprintui2.2-dev
    >= 2.12.1-3, libgail-dev >= 1.8.11-1+b1 to avoid any reference to
    Xrender.la and Xcursor.la in .la files.
    [debian/control, debian/control.in]
  * Stop shipping *.la files in libgtkhex0-dev.
    [debian/libgtkhex0-dev.install]

 -- Loic Minier <lool@dooz.org>  Sun, 23 Apr 2006 20:32:32 +0200

ghex (2.8.2-1) unstable; urgency=low

  * New upstream release, mostly translations updates and additions.
  * Add CDBS' utils, and simple-patchsys.
    [debian/rules]
  * Bump up Standards-Version to 3.6.2.
    [debian/control, debian/control.in]
  * Add a ${misc:Depends} dep to libgtkhex0-dev.
    [debian/control, debian/control.in]
  * Drop obsolete Conflicts/Replaces on libgtkhex0 and libgtkhex0-dev.
    [debian/control, debian/control.in]
  * Update download URL.
    [debian/copyright]
  * Update license.
    [debian/copyright]
  * Install schema in ghex.
    [debian/ghex.install]
  * Drop obsolete and dangerous postinst, everything is now handled by
    gnome.mk (dh_scrollkeeper and dh_gconf).
    [debian/postinst]
  * Fix quoting in menu file.
    [debian/menu]

 -- Loic Minier <lool@dooz.org>  Fri, 10 Mar 2006 14:40:06 +0100

ghex (2.8.1-1) unstable; urgency=low

  * New upstream release.

 -- Sebastien Bacher <seb128@debian.org>  Tue, 19 Oct 2004 13:20:01 +0200

ghex (2.8.0-2) unstable; urgency=low

  * Upload in unstable:
    - in fact this is not a GNOME 2.8 desktop part, this version runs fine
      with GNOME 2.6.

 -- Sebastien Bacher <seb128@debian.org>  Sun, 17 Oct 2004 16:59:37 +0200

ghex (2.8.0-1) experimental; urgency=low

  * New upstream release:
    - should fix the problems with the preferences (Closes: #251826).
    - the menu entry is in Programming now (Closes: #262394).

 -- Sebastien Bacher <seb128@debian.org>  Sun, 17 Oct 2004 14:24:39 +0200

ghex (2.6.1-1) unstable; urgency=low

  * New upstream release
    + fix libgtkhex (Closes: #246539, #248138).

 -- Sebastien Bacher <seb128@debian.org>  Thu, 27 May 2004 00:59:41 +0200

ghex (2.6.0-3) unstable; urgency=low

  * Upload in unstable.

 -- Sebastien Bacher <seb128@debian.org>  Wed, 26 May 2004 23:26:00 +0200

ghex (2.6.0-2) experimental; urgency=low

  J.H.M. Dassen (Ray) <jdassen@debian.org>:
  * [debian/control.in] Added missing build dependencies gnome-pkg-tools,
    libxml-parser-perl. (Closes: #246515)

 -- Sebastien Bacher <seb128@debian.org>  Thu, 29 Apr 2004 19:56:44 +0200

ghex (2.6.0-1) experimental; urgency=low

  * New upstream release.
  * Updated for the GNOME Team.
  * debian/watch:
    + added.

 -- Sebastien Bacher <seb128@debian.org>  Mon, 26 Apr 2004 00:21:41 +0200

ghex (2.5.0-2) unstable; urgency=low

  * Rebuilded with Gnome2.2 libs.

 -- Sebastien Bacher <seb128@debian.org>  Thu, 16 Oct 2003 17:36:29 +0200

ghex (2.5.0-1) unstable; urgency=low

  * New upstream release.

 -- Sebastien Bacher <seb128@debian.org>  Thu, 16 Oct 2003 02:49:31 +0200

ghex (2.4.0.1-3) unstable; urgency=low

  * Split package for libs (Closes: #213208).

 -- Sebastien Bacher <seb128@debian.org>  Sun,  5 Oct 2003 21:54:16 +0200

ghex (2.4.0.1-2) unstable; urgency=low

  * Upload in unstable.
  * Fixed missing depends on scrollkeeper (Closes: 211408).

 -- Sebastien Bacher <seb128@debian.org>  Mon, 22 Sep 2003 15:44:38 +0200

ghex (2.4.0.1-1) experimental; urgency=low

  * GNOME 2.4 release, uploaded in experimental.
  * Removed debian/postrm and debian/postinst, since debhelper create them.
  * Switched to cdbs.
  * Updated Build-Depends.
  * Upgrade Standards-Version to 3.6.1.0.

 -- Sebastien Bacher <seb128@debian.org>  Wed, 10 Sep 2003 21:28:44 +0200

ghex (2.2.1-1) unstable; urgency=low

  * New upstream release.
  * Changed section to gnome.
  * Updated to standards version 3.5.10.0.
  * Added Depends on ${misc:Depends}.

 -- Sebastien Bacher <seb128@debian.org>  Wed, 10 Sep 2003 21:27:52 +0200

ghex (2.2.0-2) unstable; urgency=low

  * Fixed menu entry (Closes: #183091).
  * Fixed documentation (Closes: #183090).

 -- Sebastien Bacher <seb128@debian.org>  Sun,  2 Mar 2003 12:26:56 +0100

ghex (2.2.0-1) unstable; urgency=low

  * New maintainer (according to robot101@debian.org maintainer has not
    interested in this package any more).
  * New upstream release (Closes: #178931).
    - "Add view" menu works now (Closes: #152390).
  * Bumped Standards-Version to 3.5.8.
  * Updated Build-Depends.
  * Fixed spelling error (Closes: #124669).

 -- Sebastien Bacher <seb128@debian.org>  Sat,  1 Mar 2003 14:30:08 +0100

ghex (1.2-4) unstable; urgency=low

  * Modified postinst and postrm to cope without scrollkeeper package
    (closes: #98000)

 -- Peter Joseph <kneecaps@ntlworld.com>  Sun, 27 May 2001 01:37:14 +0100

ghex (1.2-3) unstable; urgency=low

  * Modified postinst and postrm to catch errors
  * Fixed manpage description

 -- Peter Joseph <kneecaps@ntlworld.com>  Sat, 28 Apr 2001 00:29:15 +0100

ghex (1.2-2) unstable; urgency=low

  * New maintainer
  * Repackged by myself
  * Removed commented-out line from zh_CN.GB2312.po (closes: #94551)
  * Commented-out lines in omf-install/Makefile.in as scrollkeeper db
    cannot be made at build time
  * Added postinst and postrm to fix scrollkeepers db on install and
    removal
  * Fixed build-deps and description in control file
  * Fixed lintian warnings (file locations, manpage, excluded zero byte
    news file, removed upstream install docs)
  * Removed empty /usr/sbin
  * Tidied up copyright

 -- Peter Joseph <kneecaps@ntlworld.com>  Thu, 26 Apr 2001 23:48:25 +0100

ghex (1.2-1) unstable; urgency=low

  * Initial Release. (Closes: #93982)

 -- Eric Gillespie, Jr. <epg@debian.org>  Sat, 14 Apr 2001 14:50:33 -0500
